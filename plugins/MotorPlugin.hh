#ifndef MOTOR_PLUGIN_HH
#define MOTOR_PLUGIN_HH

#include "gazebo/common/common.hh"
#include "ros/ros.h"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/MultiRayShape.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/Joint.hh"
#include "gazebo/physics/PhysicsIface.hh"

#include <std_msgs/Float64.h>


namespace gazebo
{
  class GAZEBO_VISIBLE MotorPlugin : public ModelPlugin
  {
    // brief Constructor
    public: MotorPlugin();

    // brief Destructor
    public: ~MotorPlugin();

    // brief Load the plugin
    // param take in SDF root element
    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);

    public: virtual void SpeedCallback(const std_msgs::Float64::ConstPtr& msg);

    private: virtual void Update();

    private: physics::ModelPtr model;
    private: physics::WorldPtr world;

    // Connection to World Update events.
    private: event::ConnectionPtr worldConnection;

    private: std::string joint_name, command_topic, robot_namespace;

    private: double torque, update_rate, update_period, last_update_time;

    private: double motor_speed;
    private: physics::JointPtr motor_joint;

    protected: std::unique_ptr<ros::NodeHandle> rosNode;
    private: ros::Subscriber cmd_vel_subscriber;
  };
}
#endif
