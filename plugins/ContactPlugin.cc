#include "ContactPlugin.hh"

using namespace gazebo;
GZ_REGISTER_SENSOR_PLUGIN(ContactPlugin)

/////////////////////////////////////////////////
ContactPlugin::ContactPlugin() : SensorPlugin(){
}

/////////////////////////////////////////////////
ContactPlugin::~ContactPlugin(){
}

/////////////////////////////////////////////////
void ContactPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr /*_sdf*/){
  // Get the parent sensor.
  this->parentSensor = std::dynamic_pointer_cast<sensors::ContactSensor>(_sensor);

  // Make sure the parent sensor is valid.
  if (!this->parentSensor){
    std::cout << "ContactPlugin requires a ContactSensor.\n";
    return;
  }

  // Connect to the sensor update event.
  this->updateConnection = this->parentSensor->ConnectUpdated(std::bind(&ContactPlugin::OnUpdate, this));

  // Make sure the parent sensor is active.
  this->parentSensor->SetActive(true);
  this->rosNode.reset(new ros::NodeHandle());
  this->contact_pub = this->rosNode->advertise<std_msgs::Bool>("contact",1000);
}

/////////////////////////////////////////////////
void ContactPlugin::OnUpdate(){
  // Get all the contacts.
  msgs::Contacts contacts;
  
  contacts = this->parentSensor->Contacts();
  if(contacts.contact_size() != 0){
    //std::cout<< contacts.contact_size()<< std::endl;
    std_msgs::Bool msg;
    msg.data = true;
    this->contact_pub.publish(msg);
  }
  /*for (unsigned int i = 0; i < contacts.contact_size(); ++i){
    std::cout << "Collision between[" << contacts.contact(i).collision1()
              << "] and [" << contacts.contact(i).collision2() << "]\n";
  }*/
}