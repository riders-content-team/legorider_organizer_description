#ifndef LIFTER_PLUGIN_HH
#define LIFTER_PLUGIN_HH

#include "gazebo/common/common.hh"
#include "ros/ros.h"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/MultiRayShape.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/Joint.hh"
#include "gazebo/physics/PhysicsIface.hh"

#include <std_msgs/Float64.h>


namespace gazebo
{
  class GAZEBO_VISIBLE LifterPlugin : public ModelPlugin
  {
    // brief Constructor
    public: LifterPlugin();

    // brief Destructor
    public: ~LifterPlugin();

    // brief Load the plugin
    // param take in SDF root element
    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);

    public: virtual void SpeedCallback(const std_msgs::Float64::ConstPtr& msg);

    private: virtual void Update();

    private: physics::ModelPtr model;
    private: physics::WorldPtr world;

    // Connection to World Update events.
    private: event::ConnectionPtr worldConnection;

    private: std::string joint_name, command_topic, robot_namespace;

    private: double torque, update_rate, update_period, last_update_time;

    private: double lifter_speed;
    private: physics::JointPtr lifter_joint;

    protected: std::unique_ptr<ros::NodeHandle> rosNode;
    private: ros::Subscriber cmd_vel_subscriber;
  };
}
#endif
