#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Float64

def lifter_up():
    lifter_pub(1)

def lifter_down():
    lifter_pub(-1)

left_motor_pub = rospy.Publisher('/legorider_organizer/wheel_left_joint/set_speed', Float64, queue_size=10)
right_motor_pub = rospy.Publisher('/legorider_organizer/wheel_right_joint/set_speed', Float64, queue_size=10)
lifter_pub = rospy.Publisher('/legorider_organizer/lifter/set_speed', Float64, queue_size=10)

try:
    lifter_up()
except rospy.ROSInterruptException:
    pass
