cmake_minimum_required(VERSION 2.8.3)
project(legorider_organizer_description)
set(CMAKE_CXX_FLAGS "-std=c++14 ${CMAKE_CXX_FLAGS}")
find_package(gazebo REQUIRED)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  gazebo_ros
  gazebo
  message_generation
)

link_directories(${GAZEBO_LIBRARY_DIRS})

include_directories(${Boost_INCLUDE_DIR}
                    ${catkin_INCLUDE_DIRS}
                    ${GAZEBO_INCLUDE_DIRS}
                    ${roscpp_INCLUDE_DIRS}
                    ${std_msgs_INCLUDE_DIRS}
                    ${SDFormat_INCLUDE_DIRS})

add_service_files(
  FILES
  CameraService.srv
  LaserService.srv
)

generate_messages(
  DEPENDENCIES std_msgs geometry_msgs
)

# For motor plugin
add_library(MotorPlugin plugins/MotorPlugin.cc)
add_dependencies(MotorPlugin ${catkin_EXPORTED_TARGETS})
target_link_libraries(MotorPlugin ${roscpp_LIBRARIES} ${GAZEBO_LIBRARIES})

# For lifter plugin
add_library(LifterPlugin plugins/LifterPlugin.cc)
add_dependencies(LifterPlugin ${catkin_EXPORTED_TARGETS})
target_link_libraries(LifterPlugin ${roscpp_LIBRARIES} ${GAZEBO_LIBRARIES})


# camera sensor plugin
add_library(CameraSensorPlugin plugins/CameraSensorPlugin.cc)
add_dependencies(CameraSensorPlugin ${catkin_EXPORTED_TARGETS} legorider_organizer_description_generate_messages_cpp)
target_link_libraries(CameraSensorPlugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES} ${Boost_LIBRARIES})

#For laser sensor plugin
add_library(LaserSensorPlugin plugins/LaserSensorPlugin.cc)
add_dependencies(LaserSensorPlugin ${catkin_EXPORTED_TARGETS} legorider_organizer_description_generate_messages_cpp)
target_link_libraries(LaserSensorPlugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES} ${Boost_LIBRARIES})


catkin_package(
  CATKIN_DEPENDS
    message_runtime
    gazebo_ros
    std_msgs
    roscpp
  DEPENDS
    gazebo
    SDF
    roscpp
    gazebo_ros
    message_runtime
    std_msgs
)
